export default {
    places: [
        {
            imageURL: '/images/desayuno.jpg',
            title: 'BreakFast Sweet & Health',
            description: 'BreakFast Sweet & Health corporation is an American breakfast house company.'
        },
        {
            imageURL: '/images/coffe.jpg',
            title: 'StarBucks',
            description: 'StarBucks Corporation is an American coffee company and coffeehouse chain.'
        },
        {
            imageURL: '/images/pizza.jpg',
            title: 'Flash Pizza',
            description: 'Flash Pizza is an American restaurant of handmade pizza.'
        }
    ]
}