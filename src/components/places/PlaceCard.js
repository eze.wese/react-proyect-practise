import React from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import { CSSTransition } from 'react-transition-group';

export default class PlaceCard extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      show: true
    }

    
  } 
  
  render(){
        return(
          <CSSTransition 
            classNames='fade'
            in={this.props.in}
          >
            <div className="col-xs-12 col-sm-4" key={this.props.index}>
              <Card>
                <CardMedia>
                  <img src={process.env.PUBLIC_URL + this.props.place.imageURL} alt="" style={{ 'maxWidth': '100%' }} />
                </CardMedia>
                <CardHeader title={this.props.place.title}></CardHeader>
                <CardContent>{this.props.place.description}</CardContent>
                <CardActions style={{ 'placeContent': 'flex-end' }}>
                  <Button color="secondary">Ver más</Button>
                  <Button color="secondary" onClick={() => this.props.onRemove(this.props.place)}>Ocultar</Button>
                </CardActions>
              </Card>
            </div>
          </CSSTransition>
        );
    }
}