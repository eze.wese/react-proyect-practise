import React from "react";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
import yellow from "@material-ui/core/colors/yellow";

export default class Benefit extends React.Component {
  render() {
    return (
      <ul>
        <Card className="Header-Benefit">
          <CardContent>
            <div className="row">
              <div className="Header-benefit-image" style={{ backgroundColor: green["A400"] }} >
                <img src={process.env.PUBLIC_URL + "/images/manos.png"} alt="" />
              </div>
              <div className="Header-benefit-content">
                <h3>Calificacion emocional</h3>
                <p>Califica tus lugares con experiencias, no con números</p>
              </div>
            </div>
          </CardContent>
        </Card>
        <Card className="Header-Benefit">
          <CardContent>
            <div className="row">
              <div className="Header-benefit-image" style={{ backgroundColor: yellow["A400"] }}>
                <img src={process.env.PUBLIC_URL + "/images/no-internet.png"} alt="" />
              </div>
              <div className="Header-benefit-content">
                <h3>¿Sin Internet?</h3>
                <p>Funciona sin internet o conexiones lentaso</p>
              </div>
            </div>
          </CardContent>
        </Card>
        <Card className="Header-Benefit">
          <CardContent>
            <div className="row">
              <div className="Header-benefit-image" style={{ backgroundColor: red["A400"] }}>
                <img src={process.env.PUBLIC_URL + "/images/star.png"} alt="" />
              </div>
              <div className="Header-benefit-content">
                <h3>Tus lugares favoritos</h3>
                <p>Realiza una lista con tus sitios favoritos</p>
              </div>
            </div>
          </CardContent>
        </Card>
      </ul>
    );
  }
}
