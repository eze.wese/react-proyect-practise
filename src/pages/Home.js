import React from 'react';
import Button from '@material-ui/core/Button';

import indigo from '@material-ui/core/colors/indigo';

import Title from '../components/Title';
import data from '../requests/places';
import Benefits from '../components/Benefits';
import PlaceCard from '../components/places/PlaceCard';
import { TransitionGroup } from 'react-transition-group';


export default class Home extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      places: []
    }

    setTimeout(() => this.setState({places: data.places}),3000)

    this.hidePlace = this.hidePlace.bind(this);
  }

  places() {
    return this.state.places.map((place, index) => {
      return (
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
      );
    })
  }

  hidePlace(place){
    this.setState({
      places: this.state.places.filter(elem => elem !== place)
    })
  }

  render() {
    return (
      <section>
        <div className="Header-background">
          <div className="container">
            <div className="Header-main">
              <Title></Title>

              <Button variant="contained" color="secondary">Crear cuenta gratuita</Button>

              <img className="Header-illustration" src={process.env.PUBLIC_URL + '/images/logo.png'} alt="" />
            </div>
            <div>
              <Benefits></Benefits>
            </div>
          </div>
        </div>
        <div style={{ 'backgroundColor': indigo[400], 'padding': '50px' }}>
          <h3 style={{ 'fontSize': '25', 'color': 'white' }}>Sitios más Populares</h3>
          <TransitionGroup className="row">
            {this.places()}
          </TransitionGroup>
        </div>
      </section>
    );
  }
}